// -*- C++ -*-
/**
 * \file PersonalWordList.h
 * This file is part of LyX, the document processor.
 * Licence details can be found in the file COPYING.
 *
 * \author Stephan Witt
 *
 * Full author contact details are available in file CREDITS.
 */

#ifndef PERSONAL_WORD_LIST_H
#define PERSONAL_WORD_LIST_H

#include "support/strfwd.h"
#include "support/docstring_list.h"
#include "support/FileName.h"

#include <string>

namespace lyx {

/// A PersonalWordListPart holds a part of the word list with persistent state
struct PersonalWordListPart {
	/// the word list has an associated language, and a flag indicating whether it is an includes or excludes list
	PersonalWordListPart(std::string lang, bool is_includes) : lang_(lang), is_includes_(is_includes), dirty_(false) {}
	/// the location of the file to hold to word list
	lyx::support::FileName dictfile() const;
	/// (re)load the word list from file
	void load();
	/// save word list to file
	void save();
	/// check for presence of given word
	bool exists(docstring const & word) const;
	/// add given word to list
	void insert(docstring const & word);
	/// remove given word from list
	void remove(docstring const & word);
	/// is word list modified after load/save?
	bool isDirty() const { return dirty_; }
	/// first item in word list
	docstring_list::const_iterator begin() const;
	/// end of word list
	docstring_list::const_iterator end() const;
private:
	///
	docstring_list words_;
	///
	std::string lang_;
	///
	bool is_includes_;
	///
	bool dirty_;
	///
	bool equalwords(docstring const & w1, docstring const & w2) const;
	///
	std::string header() const { return is_includes_ ? "# personal word list" : "# personal world list (exclusions)";  }
	///
	void dirty(bool flag) { dirty_ = flag; }
};

/// A PersonalWordState holds a list of words to include (i.e. marked as spelt correctly), and a list of words to exclude (i.e. marked as spelled incorrectly)
class PersonalWordList {
public:
	/// the word list has an associated language
	PersonalWordList(std::string lang) : includes_(lang, true), excludes_(lang, false) {}

	/// first item in includes word list
	docstring_list::const_iterator includes_begin() const { return includes_.begin(); }
	/// end of includes word list
	docstring_list::const_iterator includes_end() const { return includes_.end(); }
	/// first item in excludes word list
	docstring_list::const_iterator excludes_begin() const { return excludes_.begin(); }
	/// end of excludes word list
	docstring_list::const_iterator excludes_end() const { return excludes_.end(); }
	/// (re)load both word lists from file
	void load() { includes_.load(); excludes_.load(); }
	/// save both word lists to file
	void save() { includes_.save(); excludes_.save(); }
	/// is the given word excluded? (i.e. we previously called remove)
	bool excluded(docstring const & word) const { return excludes_.exists(word); }
	/// is the given word included? (i.e. we previously called insert)
	bool included(docstring const & word) const { return includes_.exists(word); }
	/// insert a given word to the set of valid words
	void insert(docstring const & word) { excludes_.remove(word); includes_.insert(word); }
	/// remove given word from the set of valid words
	void remove(docstring const & word) { includes_.remove(word); excludes_.insert(word); }

private:
	/// The list of words to include
	PersonalWordListPart includes_;
	/// The list of words to exclude
	PersonalWordListPart excludes_;
};

} // namespace lyx

#endif // PERSONAL_WORD_LIST_H
